#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:134217728:d2543b984bfce1be81d825c68bcde701132b3acd; then
  applypatch  \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:100663296:59f73c2e221321ff3f939d5d15254a6810446775 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:134217728:d2543b984bfce1be81d825c68bcde701132b3acd && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
