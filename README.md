## qssi-user 10 QKQ1.200830.002 V12.0.2.0.QJQIDXM release-keys
- Manufacturer: xiaomi
- Platform: bengal
- Codename: lemon
- Brand: Redmi
- Flavor: qssi-user
- Release Version: 10
- Id: QKQ1.200830.002
- Incremental: V12.0.2.0.QJQIDXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: 440
- Fingerprint: Redmi/lemon/lemon:10/QKQ1.200830.002/V12.0.2.0.QJQIDXM:user/release-keys
- OTA version: 
- Branch: qssi-user-10-QKQ1.200830.002-V12.0.2.0.QJQIDXM-release-keys
- Repo: redmi/lemon
